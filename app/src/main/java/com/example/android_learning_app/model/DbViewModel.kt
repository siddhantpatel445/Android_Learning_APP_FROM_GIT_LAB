package com.example.android_learning_app.model

import androidx.lifecycle.ViewModel
import com.example.android_learning_app.repository.DbRepository
import com.example.android_learning_app.roomDb.Employee

class DbViewModel(private val repository: DbRepository): ViewModel() {
    suspend fun insertEmployee(employee: Employee){
        repository.insertEmployee(employee)
    }
}