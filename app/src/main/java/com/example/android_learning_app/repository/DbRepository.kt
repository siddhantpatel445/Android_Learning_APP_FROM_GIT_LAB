package com.example.android_learning_app.repository

import com.example.android_learning_app.roomDb.Employee
import com.example.android_learning_app.roomDb.EmployeeDao

class DbRepository(private val employeeDao: EmployeeDao) {

        suspend fun insertEmployee(employee: Employee){
            employeeDao.insertEmployee(employee)
        }

}