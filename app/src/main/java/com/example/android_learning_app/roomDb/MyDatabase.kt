package com.example.android_learning_app.roomDb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Employee::class], version = 1)
abstract class MyDatabase(): RoomDatabase() {
    abstract fun employeeDao(): EmployeeDao



    companion object{
        @Volatile
        var INSTANCE: MyDatabase? = null
        fun getInstance(context: Context): MyDatabase{
            var instance = INSTANCE
            if (INSTANCE == null){
                synchronized(this){
                    instance = Room.databaseBuilder(context, MyDatabase::class.java, "employeedb").build()
                    INSTANCE = instance
                }
            }
            return INSTANCE!!
        }
    }


}