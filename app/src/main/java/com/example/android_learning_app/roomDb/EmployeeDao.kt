package com.example.android_learning_app.roomDb

import androidx.room.Dao
import androidx.room.Insert

@Dao
interface EmployeeDao {
    @Insert
    suspend fun insertEmployee(vararg employee: Employee)
}