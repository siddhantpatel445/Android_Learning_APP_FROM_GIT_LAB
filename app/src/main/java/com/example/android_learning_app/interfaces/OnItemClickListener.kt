package com.example.android_learning_app.interfaces

import com.example.androidconcept_tp_5.model.Student

interface OnItemClickListener {
    fun onItemClick(position: Int, student: Student)
}