package com.example.android_learning_app.Activity

import android.app.ActionBar
import android.app.Dialog
import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android_learning_app.R
import com.example.android_learning_app.adapters.MyRecyclerViewAdapter
import com.example.android_learning_app.databinding.ActivityRecyclerviewBinding
import com.example.android_learning_app.databinding.LayoutCreateDataBinding
import com.example.android_learning_app.factory.DetailsActivityViewModelFactory
import com.example.android_learning_app.interfaces.OnItemClickListener
import com.example.android_learning_app.repository.SqliteDBRepository
import com.example.android_learning_app.viewModels.DetailsActivtyViewModel
import com.example.androidconcept_tp_5.model.Student

class RecyclerviewActivity : AppCompatActivity(), View.OnClickListener, OnItemClickListener {

    lateinit var binding: ActivityRecyclerviewBinding
    lateinit var factory: DetailsActivityViewModelFactory
    lateinit var viewModel: DetailsActivtyViewModel
    private lateinit var cursor: Cursor
    private lateinit var myAdapter: MyRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recyclerview)
        factory = DetailsActivityViewModelFactory(SqliteDBRepository(this))
        viewModel = ViewModelProvider(this, factory)[DetailsActivtyViewModel::class.java]

        binding.btnCreateData.setOnClickListener(this)
        setRecyclerview()

        myAdapter.setOnItemClickListener(this)
    }

    override fun onClick(view: View?) {
        val layoutCustomBinding = LayoutCreateDataBinding.inflate(layoutInflater)
        val dialog = Dialog(this)
        dialog.setContentView(layoutCustomBinding.root)
        dialog.setCancelable(false)
        val windowManager = WindowManager.LayoutParams()
        windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
        windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = windowManager
        dialog.show()


        layoutCustomBinding.btnSubmit.setOnClickListener {
            viewModel.createData(layoutCustomBinding.tilFname.editText?.text.toString(), layoutCustomBinding.tilLname.editText?.text.toString(), layoutCustomBinding.tilPhone.editText?.text.toString(), true)
            setRecyclerview()
            dialog.dismiss()
        }
    }

    private fun setRecyclerview() {
//        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        myAdapter = MyRecyclerViewAdapter(getAllData(), this)
        binding.recyclerView.adapter = myAdapter
        myAdapter.notifyDataSetChanged()
    }

    private fun getAllData(): ArrayList<Student>{
        cursor = viewModel.getData()
        var listOfStudent = ArrayList<Student>()
        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {
                val srNo = cursor.getString(0)
                val fName = cursor.getString(1)
                val lName = cursor.getString(2)
                val phone = cursor.getString(3)
                val student = Student(srNo, fName, lName, phone)
                listOfStudent.add(student)

            } while (cursor.moveToNext())
        } else {
            Toast.makeText(this, "Something went wrong.", Toast.LENGTH_SHORT).show()
        }

        return listOfStudent
    }

    override fun onItemClick(position: Int, student: Student) {
        Toast.makeText(this, ""+getAllData()[position], Toast.LENGTH_SHORT).show()
    }
}