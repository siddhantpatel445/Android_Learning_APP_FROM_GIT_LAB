package com.example.android_learning_app.Activity

import android.app.ActionBar
import android.app.Dialog
import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.android_learning_app.R
import com.example.android_learning_app.adapters.ListViewAdapter
import com.example.android_learning_app.databinding.ActivityDetailsBinding
import com.example.android_learning_app.databinding.LayoutCreateDataBinding
import com.example.android_learning_app.factory.DetailsActivityViewModelFactory
import com.example.android_learning_app.repository.SqliteDBRepository
import com.example.android_learning_app.viewModels.DetailsActivtyViewModel
import com.example.androidconcept_tp_5.model.Student

class DetailsActivity : AppCompatActivity(),
    View.OnClickListener, AdapterView.OnItemClickListener,
    AdapterView.OnItemLongClickListener/*, AdapterView.OnItemSelectedListener*/ {
    lateinit var binding: ActivityDetailsBinding
//    val data = arrayOf("AA", "BB", "CC", "DD", "EE")
    lateinit var factory: DetailsActivityViewModelFactory
    lateinit var viewModel: DetailsActivtyViewModel
    private var position = 0
    private lateinit var cursor: Cursor


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)

//        val dataAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data)
//        binding.spinner.adapter = dataAdapter
//
//        binding.spinner.onItemSelectedListener = this


        factory = DetailsActivityViewModelFactory(SqliteDBRepository(this))
        viewModel = ViewModelProvider(this, factory)[DetailsActivtyViewModel::class.java]

        binding.btnCreateData.setOnClickListener(this)

//        Log.d("listOfStudents", " $listOfStudents")

//        val arrayAdaper = ArrayAdapter(this, android.R.layout.simple_list_item_1, data)
//        binding.listView.adapter = arrayAdaper
        setListview()
//        binding.listView.setOnItemClickListener(this)
        registerForContextMenu(binding.listView)
        binding.listView.setOnItemLongClickListener(this)
    }

    override fun onClick(view: View?) {
        val layoutCustomBinding = LayoutCreateDataBinding.inflate(layoutInflater)
        val dialog = Dialog(this)
        dialog.setContentView(layoutCustomBinding.root)
        dialog.setCancelable(false)
        val windowManager = WindowManager.LayoutParams()
        windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
        windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = windowManager
        dialog.show()


        layoutCustomBinding.btnSubmit.setOnClickListener {
            viewModel.createData(layoutCustomBinding.tilFname.editText?.text.toString(), layoutCustomBinding.tilLname.editText?.text.toString(), layoutCustomBinding.tilPhone.editText?.text.toString(), true)
            setListview()
            dialog.dismiss()
        }
    }

    override fun onItemClick(adapter: AdapterView<*>?, view: View?, position: Int, rowItem: Long) {
        val data = adapter!!.getItemAtPosition(position).toString()
        Toast.makeText(this, "clicked $data", Toast.LENGTH_SHORT).show()
    }

//    override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, row: Long) {
//        val data = adapter?.getItemAtPosition(position)
//        Toast.makeText(this, "You are selected $data", Toast.LENGTH_SHORT).show()
//    }
//
//    override fun onNothingSelected(p0: AdapterView<*>?) {

//
//    }

    fun setListview(){
        val listOfStudents = getAllData()
        val myAdapter = ListViewAdapter(listOfStudents)
        binding.listView.adapter = myAdapter
    }

    override fun onItemLongClick(adapter: AdapterView<*>?, view: View?, position: Int, p3: Long): Boolean {
        this.position = position
        return false
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menuInflater.inflate(R.menu.options_menu, menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu_delete_all, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_delete -> {
//                Toast.makeText(this, item.title, Toast.LENGTH_SHORT).show()
                cursor.moveToPosition(position)
                val rowId = cursor.getString(0)
                viewModel.deleteSingleRecord(rowId)
                setListview()
            }

            R.id.item_update -> {
//                Toast.makeText(this, item.title, Toast.LENGTH_SHORT).show()
                cursor.moveToPosition(position)
                val rowId = cursor.getString(0)
                val fName = cursor.getString(1)
                val lName = cursor.getString(2)
                val phone = cursor.getString(3)


                val layoutCustomBinding = LayoutCreateDataBinding.inflate(layoutInflater)
                val dialog = Dialog(this)
                dialog.setContentView(layoutCustomBinding.root)
                layoutCustomBinding.btnSubmit.text = "UPDATE"
                dialog.setCancelable(false)
                val windowManager = WindowManager.LayoutParams()
                windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
                windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
                dialog.window?.attributes = windowManager
                dialog.show()

                layoutCustomBinding.etFname.setText(fName)
                layoutCustomBinding.etLname.setText(lName)
                layoutCustomBinding.etPhone.setText(phone)

                layoutCustomBinding.btnSubmit.setOnClickListener {
                    viewModel.updateData(layoutCustomBinding.tilFname.editText?.text.toString(), layoutCustomBinding.tilLname.editText?.text.toString(), layoutCustomBinding.tilPhone.editText?.text.toString(), rowId)
                    setListview()
                    dialog.dismiss()
                }
            }
        }
        return super.onContextItemSelected(item)
    }

    private fun getAllData(): ArrayList<Student>{
        cursor = viewModel.getData()
        var listOfStudent = ArrayList<Student>()
        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {
                val srNo = cursor.getString(0)
                val fName = cursor.getString(1)
                val lName = cursor.getString(2)
                val phone = cursor.getString(3)
                val student = Student(srNo, fName, lName, phone)
                listOfStudent.add(student)

            } while (cursor.moveToNext())
        } else {
            Toast.makeText(this, "Something went wrong.", Toast.LENGTH_SHORT).show()
        }

        return listOfStudent
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.item_delete_all_records){
            viewModel.deleteAllRecords()
            setListview()
        }
        return super.onOptionsItemSelected(item)
    }
}