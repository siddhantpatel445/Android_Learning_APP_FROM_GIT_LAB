package com.example.android_learning_app.Activity

import android.app.ActionBar
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.android_learning_app.R
import com.example.android_learning_app.adapters.MyRecyclerViewAdapter
import com.example.android_learning_app.databinding.ActivityRecyclerviewBinding
import com.example.android_learning_app.databinding.ActivityRoomDbBinding
import com.example.android_learning_app.databinding.LayoutCreateDataBinding
import com.example.android_learning_app.factory.DbFactory
import com.example.android_learning_app.factory.DetailsActivityViewModelFactory
import com.example.android_learning_app.model.DbViewModel
import com.example.android_learning_app.repository.DbRepository
import com.example.android_learning_app.roomDb.Employee
import com.example.android_learning_app.roomDb.MyDatabase
import com.example.android_learning_app.viewModels.DetailsActivtyViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RoomDbActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityRoomDbBinding
    lateinit var factory: DbFactory
    lateinit var viewModel: DbViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_room_db)
        factory = DbFactory(DbRepository(MyDatabase.getInstance(this).employeeDao()))
        viewModel = ViewModelProvider(this, factory)[DbViewModel::class.java]

        binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
//        myAdapter = MyRecyclerViewAdapter(getAllData(), this)
//        binding.recyclerView.adapter = myAdapter
//        myAdapter.notifyDataSetChanged()


        binding.btnCreateData.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        val layoutCustomBinding = LayoutCreateDataBinding.inflate(layoutInflater)
        val dialog = Dialog(this)
        dialog.setContentView(layoutCustomBinding.root)
        dialog.setCancelable(false)
        val windowManager = WindowManager.LayoutParams()
        windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
        windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = windowManager
        dialog.show()


        layoutCustomBinding.btnSubmit.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                viewModel.insertEmployee(Employee(1, layoutCustomBinding.tilFname.editText?.text.toString(), layoutCustomBinding.tilLname.editText?.text.toString(), layoutCustomBinding.tilPhone.editText?.text.toString()))

                withContext(Dispatchers.Main){
                    Toast.makeText(this@RoomDbActivity, "Inserted Succefully.", Toast.LENGTH_SHORT).show()
                }
            }
            dialog.dismiss()
        }
    }


}