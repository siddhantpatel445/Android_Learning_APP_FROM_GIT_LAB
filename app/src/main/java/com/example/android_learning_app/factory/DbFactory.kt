package com.example.android_learning_app.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android_learning_app.model.DbViewModel
import com.example.android_learning_app.repository.DbRepository

class DbFactory(private val repository: DbRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DbViewModel::class.java)){
            return DbViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}