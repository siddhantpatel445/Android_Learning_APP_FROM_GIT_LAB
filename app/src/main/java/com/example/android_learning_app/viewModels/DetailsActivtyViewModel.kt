package com.example.android_learning_app.viewModels

import android.database.Cursor
import androidx.lifecycle.ViewModel
import com.example.android_learning_app.repository.SqliteDBRepository
import com.example.androidconcept_tp_5.model.Student

class DetailsActivtyViewModel(private val repository: SqliteDBRepository):ViewModel() {


    fun createData(fName: String, lName: String, phone: String, status: Boolean){
        repository.createData(fName, lName, phone, status)
    }

    fun getAllData(): ArrayList<Student>{
        return repository.getAllData()
    }

    fun deleteSingleRecord(rowId: String){
        repository.deleteSingleRecord(rowId)
    }

    fun getData(): Cursor{
        return repository.getData()
    }

    fun updateData(fName: String, lName: String, phone: String, rowId: String){
        repository.updateRecord(fName, lName, phone, rowId)
    }

    fun deleteAllRecords(){
        repository.deleteAllRecords()
    }

}